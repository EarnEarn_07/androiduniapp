package com.example.uniapp.uniapp;

import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TabHost;
import android.widget.TextView;

import java.util.Timer;
import java.util.TimerTask;

import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {

    ViewPager viewPager;
    TabHost tabHost;
    //    TODO: fetch data to dataObject
    public static TextView data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        viewPager = (ViewPager) findViewById(R.id.ViewPager);
        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(this);
        viewPager.setAdapter(viewPagerAdapter);

        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new MyTimerTask(), 2000, 4000);

        data = findViewById(R.id.fetcheddata);

        fetchData process = new fetchData();
        process.execute();
    }

    /**
     * Start Main Activity
     */
    public void StartMainAct(View view) {
        Intent main = new Intent(this, MainActivity.class);
        startActivity(main);
        finish();
    }

    /**
     * Start Map Activity
     */
    public void StartMapAct(View view) {
        Intent map = new Intent(this, MapsActivity.class);
        startActivity(map);
        finish();
    }

    /**
     * Go Profile
     */
    public void StartProfile(View view) {
        Intent profile = new Intent(this, LoginActivity.class);
        startActivity(profile);
        finish();
    }

    /**
     * Go Profile
     */
    public void StartCalen(View view) {
        Intent profile = new Intent(this, CalenActivity.class);
        startActivity(profile);
        finish();
    }

    /**
     * Auto Images Slide
     */
    public class MyTimerTask extends TimerTask {
        @Override
        public void run() {
            MainActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (viewPager.getCurrentItem() == 0) {
                        viewPager.setCurrentItem(1);
                    } else if (viewPager.getCurrentItem() == 1) {
                        viewPager.setCurrentItem(2);
                    } else {
                        viewPager.setCurrentItem(0);
                    }
                }
            });
        }
    }
}
