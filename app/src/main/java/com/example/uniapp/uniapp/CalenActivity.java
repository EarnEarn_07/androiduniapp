package com.example.uniapp.uniapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class CalenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calen);
    }
    /**
     * Start Main Activity
     */
    public void StartMainAct(View view) {
        Intent main = new Intent(this, MainActivity.class);
        startActivity(main);
        finish();
    }

    /**
     * Start Map Activity
     */
    public void StartMapAct(View view) {
        Intent map = new Intent(this, MapsActivity.class);
        startActivity(map);
        finish();
    }

    /**
     * Go Profile
     */
    public void StartProfile(View view) {
        Intent profile = new Intent(this, LoginActivity.class);
        startActivity(profile);
        finish();
    }

    /**
     * Go Profile
     */
    public void StartCalen(View view) {
        Intent calen = new Intent(this, CalenActivity.class);
        startActivity(calen);
        finish();
    }
}
